package uk.co.ht.findaspot.networking.interfaces;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;
import retrofit.http.Query;
import uk.co.ht.findaspot.networking.NetConstants;

/**
 * Created by Hanson on 06/10/2015.
 */
public interface ResultsInterface {

    /**
     * Get Box Search Results
     *
     */

//    ?isinstantbook=true&nelatitude=22.37&nelongitude=-15&provider=airbnb%2Chousetrip&swlatitude=18.55&swlongitude=-160' \


    @Headers({
            "Accept: application/json",
            NetConstants.MashapeKEY_HEADER + NetConstants.MashapeKEY
    })
    @GET("/search?isinstantbook=true")
    Response GetResult(@Query("provider") String provider,
                       @Query("nelatitude") String nelatitude,
                       @Query("nelongitude") double nelongitude,
                       @Query("swlatitude") double swlatitude,
                       @Query("swlongitude") double swlongitude);

    /*
        Point search was not implemented
     */

    //Response GetResult(@Query("provider") String provider,.. etc


}
