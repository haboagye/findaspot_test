package uk.co.ht.findaspot.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uk.co.ht.findaspot.data.Result;
import uk.co.ht.findaspot.ui.fragments.DescriptionFragment;
import uk.co.ht.findaspot.ui.fragments.FacilitiesFragment;
import uk.co.ht.findaspot.ui.fragments.ReviewItemListFragment;

/**
 * Created by Hanson on 08/04/2016.
 */
public class DetailsPagerAdapter extends FragmentPagerAdapter {

    private static Result currentInfoData;

    public DetailsPagerAdapter(FragmentManager fm, Result result) {
        super(fm);
        currentInfoData = result;
    }


    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return DescriptionFragment.newInstance(currentInfoData);
            case 1:
                return FacilitiesFragment.newInstance(currentInfoData.getAmenities());
            case 2:
                return ReviewItemListFragment.newInstance(1);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Description";
            case 1:
                return "Amenities";
            case 2:
                return "Reviews";
        }
        return "";
    }
}
