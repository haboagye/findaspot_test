package uk.co.ht.findaspot.ui;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Map;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;
import uk.co.ht.findaspot.R;
import uk.co.ht.findaspot.ui.fragments.DescriptionFragment;
import uk.co.ht.findaspot.ui.fragments.FacilitiesFragment;
import uk.co.ht.findaspot.ui.fragments.ReviewItemListFragment;
import uk.co.ht.findaspot.ui.fragments.dummy.DummyContent;
import uk.co.ht.findaspot.ui.views.ImageSliderView;
import uk.co.ht.findaspot.ui.views.SpotListView;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, SpotListView.SpotListListener,
        DescriptionFragment.DescriptionFragmentListener,FacilitiesFragment.FacilitiesFragmentListener,
        ReviewItemListFragment.ReviewItemListListener
{

    public static final String TAG = "MAP ACTIVITY";


    /**
     * Public Static
     */
    public static boolean isGPSEnabled;
    public static boolean locationUpdated;
    public static SharedPreferences sharedPreferences;
    /**
     * Private Static
     */

    ImageSliderView imageSliderView;
    SpotListView infoView;
    ContentLoadingProgressBar contentLoadingProgressBar;

    /**
     * Private
     */
    private Observable<Location> lastKnownLocationObservable;
    private Observable<Location> locationUpdatesObservable;
    private ReactiveLocationProvider locationProvider;
    private Subscription lastKnownLocationSubscription;
    private Subscription updatableLocationSubscription;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        isGPSEnabled = true;
        locationUpdated = false;
        sharedPreferences = getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        infoView = (SpotListView) findViewById(R.id.lower_container);
        imageSliderView = (ImageSliderView) findViewById(R.id.photoView);
        UpdateSuggestedContent(this);
        contentLoadingProgressBar = (ContentLoadingProgressBar) findViewById(R.id.progressBar);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Snackbar.make(infoView,"Click on a marker to see more info", Snackbar.LENGTH_SHORT).show();
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                LoadListLayout();
                return false;
            }
        });
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                return false;
            }
        });
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                contentLoadingProgressBar.show();
                infoView.UpdateDrawer(mMap.getProjection().getVisibleRegion());
            }
        });


    }

    @Override
    public void OnClick() {

    }

    @Override
    public void UpdateMap(Map<String, LatLng> points) {
        contentLoadingProgressBar.hide();
        mMap.clear();
        for (Map.Entry<String, LatLng> dot : points.entrySet()) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(dot.getValue());
            markerOptions.title(dot.getKey());
            mMap.addMarker(markerOptions);
        }
        LoadListLayout();
    }

    @Override
    public void ShowPics(Map<String, String> photos) {
        imageSliderView.LoadUrls(photos);
        LoadImageSliderLayout();
    }



    @Override
    protected void onStart() {
        super.onStart();
        // if (isGPSEnabled && !sharedPreferences.contains("GPSPermission")) {
        int gpsPermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (gpsPermissionCheck == PackageManager.PERMISSION_GRANTED) {
            lastKnownLocationSubscription = lastKnownLocationObservable
                    .subscribe(new Action1<Location>() {
                        @Override
                        public void call(Location location) {

                            if (!locationUpdated) {
                                OnLocationUpdated(location);
                                mMap.setMyLocationEnabled(true);
                                locationUpdated = true;
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                                StoreSuggestions(location);
                            }
                        }
                    });
            updatableLocationSubscription = locationUpdatesObservable
                    .subscribe(new Action1<Location>() {
                        @Override
                        public void call(Location location) {

                            if (!locationUpdated) {
                                OnLocationUpdated(location);
                                mMap.setMyLocationEnabled(true);
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                                locationUpdated = true;
                                StoreSuggestions(location);
                            }
                        }
                    });
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    2001);
        }
        // do not ask for permission again
        //     sharedPreferences.edit().putBoolean("GPSPermission", true).commit();
        // }
    }

    private void OnLocationUpdated(Location location) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 2001) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);

            } else {

            }
        }
    }

    public void UpdateSuggestedContent(final Context context) {
        locationProvider = new ReactiveLocationProvider(context);
        lastKnownLocationObservable = locationProvider.getLastKnownLocation();

        final LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(20000);
        locationUpdatesObservable = locationProvider
                .checkLocationSettings(
                        new LocationSettingsRequest.Builder()
                                .addLocationRequest(locationRequest)
                                .setAlwaysShow(true)
                                .build()
                )
                .doOnNext(new Action1<LocationSettingsResult>() {
                    @Override
                    public void call(LocationSettingsResult locationSettingsResult) {
                        Status status = locationSettingsResult.getStatus();
                        if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                            try {
                                status.startResolutionForResult((Activity) context, 1110);
                                isGPSEnabled = true;

                                mMap.setMyLocationEnabled(true);
                                Location location = mMap.getMyLocation();
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                            } catch (Exception ex) {
                                Log.e("MainActivity", "Error ", ex);
                            }
                        }
                    }
                })
                .flatMap(new Func1<LocationSettingsResult, Observable<Location>>() {
                    @Override
                    public Observable<Location> call(LocationSettingsResult locationSettingsResult) {
                        return locationProvider.getUpdatedLocation(locationRequest);
                    }
                });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isGPSEnabled) {
            if (updatableLocationSubscription != null) {
                updatableLocationSubscription.unsubscribe();
            }
            if (lastKnownLocationSubscription != null) {
                lastKnownLocationSubscription.unsubscribe();
            }
        }

    }

    private void StoreSuggestions(Location location) {
        // store info in db
    }

    public void LoadListLayout() {
        if (infoView.getVisibility() != View.VISIBLE) {
            infoView.ResetView();
            infoView.setY(infoView.getY() + infoView.getHeight()*2);
            infoView.animate()
                    .alphaBy(1.0f)
                    .translationY(infoView.getHeight())
                    .setDuration(1200)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            infoView.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
                    .start();

        }

    }

    @Override
    public void onBackPressed() {
        if (imageSliderView.getVisibility() == View.INVISIBLE && infoView.getVisibility() == View.INVISIBLE)
        {
            super.onBackPressed();
        }
        else {
            if (imageSliderView.getVisibility() == View.VISIBLE) {
                imageSliderView.setVisibility(View.INVISIBLE);
                infoView.setVisibility(View.INVISIBLE);
            } else {
                if (infoView.getVisibility() == View.VISIBLE) {
                    infoView.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    public void LoadImageSliderLayout()
    {
        if (imageSliderView.getVisibility() != View.VISIBLE) {
            imageSliderView.setY(-imageSliderView.getY());
            imageSliderView.animate()
                    .alphaBy(1.0f)
                    .translationY(0)
                    .setDuration(1200)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            imageSliderView.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
                    .start();
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }
}
