
package uk.co.ht.findaspot.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.workday.postman.Postman;
import com.workday.postman.annotations.Parceled;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
@Parceled
public class Attr implements Parcelable{

    private RoomType roomType;
    private PropType propType;
    private Double bathrooms;
    private String description;
    private Boolean instantBookable;
    private ExtraGuests extraGuests;
    private Integer bedrooms;
    private Integer occupancy;
    private Cancellation cancellation;
    private Integer beds;
    private Boolean isCalAvailable;
    private String responseTime;
    private List<Fee> fees = new ArrayList<Fee>();
    private Integer lastUpdatedAt;
    private String heading;
    private Integer securityDeposit;
    private String checkOut;
    private String checkIn;
    private Integer size;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public static final Creator<Attr> CREATOR = Postman.getCreator(Attr.class);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Postman.writeToParcel(this,dest);
    }
    /**
     * 
     * @return
     *     The roomType
     */
    public RoomType getRoomType() {
        return roomType;
    }

    /**
     * 
     * @param roomType
     *     The roomType
     */
    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    /**
     * 
     * @return
     *     The propType
     */
    public PropType getPropType() {
        return propType;
    }

    /**
     * 
     * @param propType
     *     The propType
     */
    public void setPropType(PropType propType) {
        this.propType = propType;
    }

    /**
     * 
     * @return
     *     The bathrooms
     */
    public Double getBathrooms() {
        return bathrooms;
    }

    /**
     * 
     * @param bathrooms
     *     The bathrooms
     */
    public void setBathrooms(Double bathrooms) {
        this.bathrooms = bathrooms;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The instantBookable
     */
    public Boolean getInstantBookable() {
        return instantBookable;
    }

    /**
     * 
     * @param instantBookable
     *     The instantBookable
     */
    public void setInstantBookable(Boolean instantBookable) {
        this.instantBookable = instantBookable;
    }

    /**
     * 
     * @return
     *     The extraGuests
     */
    public ExtraGuests getExtraGuests() {
        return extraGuests;
    }

    /**
     * 
     * @param extraGuests
     *     The extraGuests
     */
    public void setExtraGuests(ExtraGuests extraGuests) {
        this.extraGuests = extraGuests;
    }

    /**
     * 
     * @return
     *     The bedrooms
     */
    public Integer getBedrooms() {
        return bedrooms;
    }

    /**
     * 
     * @param bedrooms
     *     The bedrooms
     */
    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    /**
     * 
     * @return
     *     The occupancy
     */
    public Integer getOccupancy() {
        return occupancy;
    }

    /**
     * 
     * @param occupancy
     *     The occupancy
     */
    public void setOccupancy(Integer occupancy) {
        this.occupancy = occupancy;
    }

    /**
     * 
     * @return
     *     The cancellation
     */
    public Cancellation getCancellation() {
        return cancellation;
    }

    /**
     * 
     * @param cancellation
     *     The cancellation
     */
    public void setCancellation(Cancellation cancellation) {
        this.cancellation = cancellation;
    }

    /**
     * 
     * @return
     *     The beds
     */
    public Integer getBeds() {
        return beds;
    }

    /**
     * 
     * @param beds
     *     The beds
     */
    public void setBeds(Integer beds) {
        this.beds = beds;
    }

    /**
     * 
     * @return
     *     The isCalAvailable
     */
    public Boolean getIsCalAvailable() {
        return isCalAvailable;
    }

    /**
     * 
     * @param isCalAvailable
     *     The isCalAvailable
     */
    public void setIsCalAvailable(Boolean isCalAvailable) {
        this.isCalAvailable = isCalAvailable;
    }

    /**
     * 
     * @return
     *     The responseTime
     */
    public String getResponseTime() {
        return responseTime;
    }

    /**
     * 
     * @param responseTime
     *     The responseTime
     */
    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    /**
     * 
     * @return
     *     The fees
     */
    public List<Fee> getFees() {
        return fees;
    }

    /**
     * 
     * @param fees
     *     The fees
     */
    public void setFees(List<Fee> fees) {
        this.fees = fees;
    }

    /**
     * 
     * @return
     *     The lastUpdatedAt
     */
    public Integer getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    /**
     * 
     * @param lastUpdatedAt
     *     The lastUpdatedAt
     */
    public void setLastUpdatedAt(Integer lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    /**
     * 
     * @return
     *     The heading
     */
    public String getHeading() {
        return heading;
    }

    /**
     * 
     * @param heading
     *     The heading
     */
    public void setHeading(String heading) {
        this.heading = heading;
    }

    /**
     * 
     * @return
     *     The securityDeposit
     */
    public Integer getSecurityDeposit() {
        return securityDeposit;
    }

    /**
     * 
     * @param securityDeposit
     *     The securityDeposit
     */
    public void setSecurityDeposit(Integer securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    /**
     * 
     * @return
     *     The checkOut
     */
    public String getCheckOut() {
        return checkOut;
    }

    /**
     * 
     * @param checkOut
     *     The checkOut
     */
    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    /**
     * 
     * @return
     *     The checkIn
     */
    public String getCheckIn() {
        return checkIn;
    }

    /**
     * 
     * @param checkIn
     *     The checkIn
     */
    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    /**
     * 
     * @return
     *     The size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * 
     * @param size
     *     The size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
