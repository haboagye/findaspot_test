package uk.co.ht.findaspot.ui.views;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.Map;

import uk.co.ht.findaspot.R;

/**
 * Created by Hanson on 08/04/2016.
 */
public class ImageSliderView extends RelativeLayout {

    Context ctx;

    SliderLayout photoSlider;

    public ImageSliderView(Context context) {
        super(context);
    }

    public ImageSliderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ctx = context;
        SetUpView();
    }

    private void SetUpView() {

        LayoutInflater layoutInflater =
                (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.photo_view_layout, this);
        photoSlider = (SliderLayout) findViewById(R.id.slider);
        photoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        photoSlider.setCustomAnimation(new DescriptionAnimation());
        photoSlider.setDuration(4000);
    }

    public void LoadUrls(Map<String,String> photoUrls)
    {
        photoSlider.removeAllSliders();
        for(String name : photoUrls.keySet()){
            TextSliderView textSliderView = new TextSliderView(ctx);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(photoUrls.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            photoSlider.addSlider(textSliderView);
        }
    }
}
