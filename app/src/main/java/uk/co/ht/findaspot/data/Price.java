
package uk.co.ht.findaspot.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.workday.postman.Postman;
import com.workday.postman.annotations.Parceled;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
@Parceled
public class Price implements Parcelable {

    private Integer monthly;
    private Integer nightly;
    private Integer maxNight;
    private Integer weekend;
    private Integer minNight;
    private Integer weekly;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public static final Parcelable.Creator<Price> CREATOR = Postman.getCreator(Price.class);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Postman.writeToParcel(this,dest);
    }
    /**
     * 
     * @return
     *     The monthly
     */
    public Integer getMonthly() {
        return monthly;
    }

    /**
     * 
     * @param monthly
     *     The monthly
     */
    public void setMonthly(Integer monthly) {
        this.monthly = monthly;
    }

    /**
     * 
     * @return
     *     The nightly
     */
    public Integer getNightly() {
        return nightly;
    }

    /**
     * 
     * @param nightly
     *     The nightly
     */
    public void setNightly(Integer nightly) {
        this.nightly = nightly;
    }

    /**
     * 
     * @return
     *     The maxNight
     */
    public Integer getMaxNight() {
        return maxNight;
    }

    /**
     * 
     * @param maxNight
     *     The maxNight
     */
    public void setMaxNight(Integer maxNight) {
        this.maxNight = maxNight;
    }

    /**
     * 
     * @return
     *     The weekend
     */
    public Integer getWeekend() {
        return weekend;
    }

    /**
     * 
     * @param weekend
     *     The weekend
     */
    public void setWeekend(Integer weekend) {
        this.weekend = weekend;
    }

    /**
     * 
     * @return
     *     The minNight
     */
    public Integer getMinNight() {
        return minNight;
    }

    /**
     * 
     * @param minNight
     *     The minNight
     */
    public void setMinNight(Integer minNight) {
        this.minNight = minNight;
    }

    /**
     * 
     * @return
     *     The weekly
     */
    public Integer getWeekly() {
        return weekly;
    }

    /**
     * 
     * @param weekly
     *     The weekly
     */
    public void setWeekly(Integer weekly) {
        this.weekly = weekly;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
