package uk.co.ht.findaspot.util;

/**
 * Created by Hanson on 08/04/2016.
 */
public class InfoPoint {

    public InfoPoint(boolean available, String name)
    {
        isAvailable = available;
        placeName = name;
    }

    public boolean isAvailable;
    public String placeName;
}
