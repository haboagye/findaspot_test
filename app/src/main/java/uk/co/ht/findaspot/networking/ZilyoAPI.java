package uk.co.ht.findaspot.networking;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.VisibleRegion;
import com.octo.android.robospice.SpiceManager;

import uk.co.ht.findaspot.networking.listeners.ResultListener;
import uk.co.ht.findaspot.networking.requests.ResultRequest;
import uk.co.ht.findaspot.networking.services.Zilyo_API_Service;

/**
 * Created by Hanson on 30/10/2015.
 */
public class ZilyoAPI {

    public static final String TAG = "API";


    private static SpiceManager spiceFanhoodManager;
    private static ZilyoAPI foxInstance;
    private static Context ctx;

    /**
     * New instance fox api.
     *
     * @param context the context
     * @return the fox api
     */
    public static ZilyoAPI NewInstance(Context context) {
        foxInstance = new ZilyoAPI(context);
        spiceFanhoodManager = new SpiceManager(Zilyo_API_Service.class);
        return foxInstance;
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ZilyoAPI getInstance() {
        return foxInstance;
    }

    private ZilyoAPI(Context context) {
        ctx = context;
    }

    /**
     * Gets spice fanhood manager.
     *
     * @return the spice fanhood manager
     */
// FANHOOD Manager
    public static SpiceManager getSpiceFanhoodManager() {
        return spiceFanhoodManager;
    }


    /**
     * On start.
     */
    public void onStart() {
        if (!spiceFanhoodManager.isStarted()) {
            spiceFanhoodManager.start(ctx.getApplicationContext());
        }
    }

    /**
     * On stop.
     */
    public void onStop() {
        if (spiceFanhoodManager.isStarted() &&
                spiceFanhoodManager.getPendingRequestCount() == 0) {
            spiceFanhoodManager.shouldStop();
        }
    }


    /**
     * Get Results.
     */
    public void GetResults(boolean spot, VisibleRegion latLng, ResultListener resultListener) {
        // here we could implement cache but there is no time

        spiceFanhoodManager.execute(new ResultRequest(spot,latLng), resultListener);
    }

/*
    public void DeleteUserPreferences(PreferenceListener preferenceListener, boolean rivals, String teamTag) {
        String userID = DrawerMenu.currentUserProfile.getUserID();
        if (!spiceFanhoodManager.isStarted())
            spiceFanhoodManager.start(ctx);
        if (rivals) {
            getSpiceFanhoodManager().execute(new PreferencesRequest(userID, teamTag, true, false, PreferencesRequest.DELETE), preferenceListener);
        }
        else {
            getSpiceFanhoodManager().execute(new PreferencesRequest(userID, teamTag, false, false, PreferencesRequest.DELETE), preferenceListener);
        }
    }*/
}
