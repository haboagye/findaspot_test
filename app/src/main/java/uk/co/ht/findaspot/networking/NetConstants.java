package uk.co.ht.findaspot.networking;

/**
 * Created by Hanson on 08/04/2016.
 */
public class NetConstants {

    public static final String BaseZilyoUrl = "https://zilyo.p.mashape.com";
    public static final String MashapeKEY = "A3bZCAKw2Mmshi0bKPn7pet9Kjatp16jylEjsnOpObgA6a8wk1";
    public static final String MashapeKEY_HEADER = "X-Mashape-Key:";

    public static final String AIRBNB = "airbnb";

    public static final String ALWAYS = "alwaysonvacation";
    public static final String APPAPART =    "apartmentsapart";
    public static final String HOMEAWAY =    "homeaway";
    public static final String HOMESTAY =    "homestay";

/*
            "bedycasa"

            "bookingpal"

            "citiesreference"

            "edomizil"

            "geronimo"

            "gloveler"

            "holidayvelvet"


            "hostelworld"

            "housetrip"

            "interhome"

            "nflats"

            "roomorama"

            "stopsleepgo"

            "theotherhome"

            "travelmob"

            "vacationrentalpeople"

            "vaycayhero"

            "waytostay"

            "webchalet"

            "zaranga"*/



}
