package uk.co.ht.findaspot.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import uk.co.ht.findaspot.R;
import uk.co.ht.findaspot.data.Amenity;
import uk.co.ht.findaspot.data.Result;

public class FacilitiesFragment extends Fragment {
    private FacilitiesFragmentListener mListener;


    public FacilitiesFragment() {
        // Required empty public constructor
    }
    public static FacilitiesFragment newInstance(List<Amenity> infoList) {
        FacilitiesFragment fragment = new FacilitiesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_facilities, container, false);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FacilitiesFragmentListener) {
            mListener = (FacilitiesFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FacilitiesFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface FacilitiesFragmentListener {
        void onFragmentInteraction(Uri uri);
    }

    public static void LoadData(List<Amenity> amenities)
    {

    }
}
