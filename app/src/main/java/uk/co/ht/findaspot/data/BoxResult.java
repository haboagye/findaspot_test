
package uk.co.ht.findaspot.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.workday.postman.Postman;
import com.workday.postman.annotations.Parceled;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
@Parceled
public class BoxResult implements Parcelable{

    private List<String> ids = new ArrayList<String>();
    private String queryType;
    private Integer status;
    private Integer page;
    private Integer resultsPerPage;
    private Integer amtOfNights;
    private String sort;
    private Boolean isNearQuery;
    private Integer runtime;
    private List<Result> result = new ArrayList<Result>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public static final Creator<BoxResult> CREATOR = Postman.getCreator(BoxResult.class);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Postman.writeToParcel(this,dest);
    }
    /**
     * 
     * @return
     *     The ids
     */
    public List<String> getIds() {
        return ids;
    }

    /**
     * 
     * @param ids
     *     The ids
     */
    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    /**
     * 
     * @return
     *     The queryType
     */
    public String getQueryType() {
        return queryType;
    }

    /**
     * 
     * @param queryType
     *     The queryType
     */
    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    /**
     * 
     * @return
     *     The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * 
     * @param page
     *     The page
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * 
     * @return
     *     The resultsPerPage
     */
    public Integer getResultsPerPage() {
        return resultsPerPage;
    }

    /**
     * 
     * @param resultsPerPage
     *     The resultsPerPage
     */
    public void setResultsPerPage(Integer resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    /**
     * 
     * @return
     *     The amtOfNights
     */
    public Integer getAmtOfNights() {
        return amtOfNights;
    }

    /**
     * 
     * @param amtOfNights
     *     The amtOfNights
     */
    public void setAmtOfNights(Integer amtOfNights) {
        this.amtOfNights = amtOfNights;
    }

    /**
     * 
     * @return
     *     The sort
     */
    public String getSort() {
        return sort;
    }

    /**
     * 
     * @param sort
     *     The sort
     */
    public void setSort(String sort) {
        this.sort = sort;
    }

    /**
     * 
     * @return
     *     The isNearQuery
     */
    public Boolean getIsNearQuery() {
        return isNearQuery;
    }

    /**
     * 
     * @param isNearQuery
     *     The isNearQuery
     */
    public void setIsNearQuery(Boolean isNearQuery) {
        this.isNearQuery = isNearQuery;
    }

    /**
     * 
     * @return
     *     The runtime
     */
    public Integer getRuntime() {
        return runtime;
    }

    /**
     * 
     * @param runtime
     *     The runtime
     */
    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    /**
     * 
     * @return
     *     The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
