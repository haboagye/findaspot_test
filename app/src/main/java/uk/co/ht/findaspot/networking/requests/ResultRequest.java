package uk.co.ht.findaspot.networking.requests;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.mime.TypedInput;
import uk.co.ht.findaspot.data.BoxResult;
import uk.co.ht.findaspot.data.Result;
import uk.co.ht.findaspot.networking.NetConstants;
import uk.co.ht.findaspot.networking.interfaces.ResultsInterface;

/**
 * Created by Hanson on 01/10/2015.
 */
public class ResultRequest extends BaseRequest<BoxResult, ResultsInterface> {

    private int booxDimention;
    private boolean isSpot;
    private VisibleRegion target;
    /**
     * Instantiates a new League list request.
     */
    public ResultRequest(boolean spot, VisibleRegion latLng) {
        super(BoxResult.class, ResultsInterface.class);
        isSpot = spot;
        target = latLng;

    }

    @Override
    public BoxResult loadDataFromNetwork() throws Exception {
        Response response = null;
        response = getService().GetResult( NetConstants.AIRBNB + "," + NetConstants.ALWAYS,
                String.format("%.3f", target.farRight.latitude),
                target.farRight.longitude,
                target.nearLeft.latitude,
                target.nearLeft.longitude);

        /*if (subLeagueID == 0) {
            response = getService().GetResult(currentSport, DNAConstants.WIS_APIKEY);
        } else {
            response = getService().GetResult(currentSport, DNAConstants.WIS_APIKEY, subLeagueID);
        }
        String expiresHeader = "";

        for (Header header : response.getHeaders()) {
            if (header.getName() != null) {
                if (header.getName().equals("Expires")) {
                    expiresHeader = header.getValue();
                    break;
                }
            }
        }
        FoxAPI.getAppToken().setExpires(expiresHeader);*/
        BoxResult boxResult;
        Gson gson = new GsonBuilder().create();
        boxResult = gson.fromJson(convertArray(response.getBody()), BoxResult.class);
        return boxResult;
    }

    private String convertArray(TypedInput body) {

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }

            // Prints the correct String representation of body.
            return out.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
