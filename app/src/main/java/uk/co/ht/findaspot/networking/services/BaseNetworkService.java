package uk.co.ht.findaspot.networking.services;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import java.util.HashMap;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * Created by Hanson on 08/04/2016.
 */
public abstract class BaseNetworkService extends RetrofitGsonSpiceService {

    protected Map<Class<?>, RestAdapter> mRetrofitInterfaceToRestAdapter;
    protected Map<Class<?>, Object> mRetrofitInterfaceToServiceMap = new HashMap<Class<?>, Object>();

    private RestAdapter mDefaultRestAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected String getServerUrl() {
        //because we create RestAdapter ourselves
        return null;
    }

    /**
     * Create default restAdapter
     *
     * @return rest adapter
     */
    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = createDefaultRestAdapterBuilder();
        mDefaultRestAdapter = builder.build();
        return builder;
    }

    protected abstract RestAdapter.Builder createDefaultRestAdapterBuilder();

    protected void addRetrofitInterface(Class<?> serviceClass, RestAdapter restAdapter) {
        if (mRetrofitInterfaceToRestAdapter == null)
            mRetrofitInterfaceToRestAdapter = new HashMap<Class<?>, RestAdapter>();
        mRetrofitInterfaceToRestAdapter.put(serviceClass, restAdapter);

    }

    @Override
    protected <T> T getRetrofitService(Class<T> serviceClass) {
        T service = (T) mRetrofitInterfaceToServiceMap.get(serviceClass);
        if (service == null) {
            service = createRetrofitService(serviceClass);
            mRetrofitInterfaceToServiceMap.put(serviceClass, service);
        }
        return service;
    }

    protected <T> T createRetrofitService(Class<T> serviceClass) {
        if (mRetrofitInterfaceToRestAdapter != null && mRetrofitInterfaceToRestAdapter.containsKey(serviceClass)) {
            RestAdapter restAdapter = mRetrofitInterfaceToRestAdapter.get(serviceClass);
            return restAdapter.create(serviceClass);
        } else {
            return mDefaultRestAdapter.create(serviceClass);
        }
    }


}
