package uk.co.ht.findaspot.util;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import uk.co.ht.findaspot.R;

/**
 * Created by Hanson on 08/04/2016.
 */
public class PointsInfoAdapter extends RecyclerView.Adapter<PointsInfoAdapter.InfoHolder> {


    public interface ItemClickListener {
        public void onItemClick(String index, int position);
    }
    // universal click method
    private static ItemClickListener itemClickListener;
    ArrayList<InfoPoint> infoPoints;

    public PointsInfoAdapter(ArrayList<InfoPoint> infoPointArrayList) {
        this.infoPoints = infoPointArrayList;
    }

    public PointsInfoAdapter() {
        infoPoints = new ArrayList<>();
    }

    @Override
    public PointsInfoAdapter.InfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);

        InfoHolder vh = new InfoHolder(v);
        return vh;
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(PointsInfoAdapter.InfoHolder holder, int position) {
        holder.textName.setText(infoPoints.get(position).placeName);
        holder.checkAvailable.setChecked(infoPoints.get(position).isAvailable);
    }

    @Override
    public int getItemCount() {
        return infoPoints.size();
    }

    public class InfoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textName;
        public CheckBox checkAvailable;

        public InfoHolder(View itemView) {
            super(itemView);
            textName = (TextView) itemView.findViewById(R.id.textView);
            checkAvailable = (CheckBox) itemView.findViewById(R.id.checkBox);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick("asfa",getPosition());
        }
    }
}
