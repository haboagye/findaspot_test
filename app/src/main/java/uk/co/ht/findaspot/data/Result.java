
package uk.co.ht.findaspot.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.workday.postman.Postman;
import com.workday.postman.annotations.Parceled;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
@Parceled
public class Result implements Parcelable{

    private String id;
    private List<Double> latLng = new ArrayList<Double>();
    private String itemStatus;
    private Attr attr;
    private Price price;
    private List<Photo> photos = new ArrayList<Photo>();
    private Location location;
    private Provider provider;
    private List<Amenity> amenities = new ArrayList<Amenity>();
    private Reviews reviews;
    private List<PriceRange> priceRange = new ArrayList<PriceRange>();
    private List<Availability> availability = new ArrayList<Availability>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The latLng
     */
    public List<Double> getLatLng() {
        return latLng;
    }

    /**
     * 
     * @param latLng
     *     The latLng
     */
    public void setLatLng(List<Double> latLng) {
        this.latLng = latLng;
    }

    /**
     * 
     * @return
     *     The itemStatus
     */
    public String getItemStatus() {
        return itemStatus;
    }

    /**
     * 
     * @param itemStatus
     *     The itemStatus
     */
    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    /**
     * 
     * @return
     *     The attr
     */
    public Attr getAttr() {
        return attr;
    }

    /**
     * 
     * @param attr
     *     The attr
     */
    public void setAttr(Attr attr) {
        this.attr = attr;
    }

    /**
     * 
     * @return
     *     The price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * 
     * @param photos
     *     The photos
     */
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    /**
     * 
     * @return
     *     The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * 
     * @param location
     *     The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * 
     * @return
     *     The provider
     */
    public Provider getProvider() {
        return provider;
    }

    /**
     * 
     * @param provider
     *     The provider
     */
    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    /**
     * 
     * @return
     *     The amenities
     */
    public List<Amenity> getAmenities() {
        return amenities;
    }

    /**
     * 
     * @param amenities
     *     The amenities
     */
    public void setAmenities(List<Amenity> amenities) {
        this.amenities = amenities;
    }

    /**
     * 
     * @return
     *     The reviews
     */
    public Reviews getReviews() {
        return reviews;
    }

    /**
     * 
     * @param reviews
     *     The reviews
     */
    public void setReviews(Reviews reviews) {
        this.reviews = reviews;
    }

    /**
     * 
     * @return
     *     The priceRange
     */
    public List<PriceRange> getPriceRange() {
        return priceRange;
    }

    /**
     * 
     * @param priceRange
     *     The priceRange
     */
    public void setPriceRange(List<PriceRange> priceRange) {
        this.priceRange = priceRange;
    }

    /**
     * 
     * @return
     *     The availability
     */
    public List<Availability> getAvailability() {
        return availability;
    }

    /**
     * 
     * @param availability
     *     The availability
     */
    public void setAvailability(List<Availability> availability) {
        this.availability = availability;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public static final Creator<Result> CREATOR = Postman.getCreator(Result.class);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Postman.writeToParcel(this,dest);
    }
}
