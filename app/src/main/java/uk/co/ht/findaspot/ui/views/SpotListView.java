package uk.co.ht.findaspot.ui.views;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ViewSwitcher;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.VisibleRegion;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.co.ht.findaspot.R;
import uk.co.ht.findaspot.data.BoxResult;
import uk.co.ht.findaspot.data.Photo;
import uk.co.ht.findaspot.data.Result;
import uk.co.ht.findaspot.networking.ZilyoAPI;
import uk.co.ht.findaspot.networking.listeners.ResultListener;
import uk.co.ht.findaspot.util.DetailsPagerAdapter;
import uk.co.ht.findaspot.util.InfoPoint;
import uk.co.ht.findaspot.util.PointsInfoAdapter;

public class SpotListView extends RelativeLayout implements PointsInfoAdapter.ItemClickListener {


    public interface SpotListListener {
        void OnClick();

        void UpdateMap(Map<String, LatLng> points);

        void ShowPics(Map<String, String> photos);

    }

    Context ctx;

    private static ZilyoAPI zilyoAPI;
    private VisibleRegion posCenter;
    private SpotListListener mListener;
    private Map<String, String> viewImages = new HashMap<>();
    private PointsInfoAdapter pointsInfoAdapter;
    private RecyclerView.LayoutManager layoutManager;

    ViewSwitcher viewSwitcher;
    ViewPager infoViewPager;
    boolean shouldReset;
    RecyclerView listRecycleView;
    public static BoxResult currentResultSet;

    public SpotListView(Context context) {
        super(context);

    }

    public SpotListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ctx = context;
        mListener = (SpotListListener) context;
        shouldReset = false;
        SetUpView();
    }

    private void SetUpView() {

        LayoutInflater layoutInflater =
                (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.fragment_spot_list, this);
        listRecycleView = (RecyclerView) findViewById(R.id.list_recycleview);
        layoutManager = new LinearLayoutManager(ctx);
        listRecycleView.setLayoutManager(layoutManager);
        pointsInfoAdapter = new PointsInfoAdapter();
        listRecycleView.setAdapter(pointsInfoAdapter);
        infoViewPager = (ViewPager) findViewById(R.id.pager);
        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        viewSwitcher.setInAnimation(ctx,R.anim.fade_in);


    }

    public void ResetView()
    {
        if (shouldReset) {
            viewSwitcher.showNext();
        }
    }

    public void UpdateDrawer(VisibleRegion targetZone) {
        posCenter = targetZone;

        if (zilyoAPI == null)
            zilyoAPI = ZilyoAPI.NewInstance(ctx);

        zilyoAPI.onStart();
        zilyoAPI.GetResults(true, posCenter, new ResultListener(this) {
            @Override
            public void onRequestFailure(SpiceException spiceException) {

            }

            @Override
            public void onRequestSuccess(BoxResult boxResult) {
                // update list adapter
                currentResultSet = boxResult;
                Map<String, LatLng> viewPoints = new HashMap<>();
                ArrayList<InfoPoint> infoPoints = new ArrayList();
                for (Result result : boxResult.getResult()) {
                    String name = result.getAttr().getHeading();
                    LatLng latLng = new LatLng(result.getLatLng().get(0),
                            result.getLatLng().get(1));
                    viewPoints.put(name, latLng);

                    infoPoints.add(new InfoPoint(result.getAttr().getIsCalAvailable(), name));
                }
                UpdateMap(viewPoints);
                // create points

                pointsInfoAdapter = new PointsInfoAdapter(infoPoints);
                pointsInfoAdapter.setOnItemClickListener(SpotListView.this);
                listRecycleView.setAdapter(pointsInfoAdapter);
                pointsInfoAdapter.notifyDataSetChanged();


            }
        });
    }


    @Override
    public void onItemClick(String index, int position) {
        Result infoResult = currentResultSet.getResult().get(position);
        Map<String, String> photoLinks = new HashMap<>();
        for (Photo photo : infoResult.getPhotos()) {
            if (photoLinks.size() < 10) {
                // we need a limit because the UI is just experimental for now - it would need more work
                // to show more than 10 images - we would need to change the paging widget to indicate
                // 16 images better than just dots
                photoLinks.put(photo.getCaption(), photo.getLarge());
            } else {
                break;
            }
        }
        mListener.ShowPics(photoLinks);
        LoadDetailView(infoResult);
    }

    /*
        This is here so it can be reused by a call to the database
     */
    public void UpdateMap(Map<String, LatLng> points) {
        mListener.UpdateMap(points);
    }


    private void LoadDetailView(Result infoResult) {
        DetailsPagerAdapter detailsPagerAdapter = new DetailsPagerAdapter(((FragmentActivity)ctx).getSupportFragmentManager(), infoResult);
        infoViewPager.setAdapter(detailsPagerAdapter);
        viewSwitcher.showNext();
        shouldReset = true;
    }

}
