package uk.co.ht.findaspot.networking.services;


import retrofit.RestAdapter;
import uk.co.ht.findaspot.networking.NetConstants;

/**
 * Created by Hanson on 01/10/2015.
 */
public class Zilyo_API_Service extends BaseNetworkService {

    /**
     * Need to make this class allow dynamic end point selection
     */

    //protected static RestAdapter.Builder mBuilder;
    @Override
    protected RestAdapter.Builder createDefaultRestAdapterBuilder() {

        /*mBuilder = new RestAdapter.Builder();
        return mBuilder;*/

        return new RestAdapter.Builder()
                .setEndpoint(NetConstants.BaseZilyoUrl);
    }

    public static RestAdapter.Builder setEndPoint(String endpoint)
    {

        return  new RestAdapter.Builder().setEndpoint(endpoint);
    }


}
