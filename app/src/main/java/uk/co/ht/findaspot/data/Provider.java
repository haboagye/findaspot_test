
package uk.co.ht.findaspot.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.workday.postman.Postman;
import com.workday.postman.annotations.Parceled;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
@Parceled
public class Provider implements Parcelable{

    private String domain;
    private String full;
    private String cid;
    private String url;
    private String nid;
    private String micro;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public static final Parcelable.Creator<Provider> CREATOR = Postman.getCreator(Provider.class);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Postman.writeToParcel(this,dest);
    }
    /**
     * 
     * @return
     *     The domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * 
     * @param domain
     *     The domain
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * 
     * @return
     *     The full
     */
    public String getFull() {
        return full;
    }

    /**
     * 
     * @param full
     *     The full
     */
    public void setFull(String full) {
        this.full = full;
    }

    /**
     * 
     * @return
     *     The cid
     */
    public String getCid() {
        return cid;
    }

    /**
     * 
     * @param cid
     *     The cid
     */
    public void setCid(String cid) {
        this.cid = cid;
    }

    /**
     * 
     * @return
     *     The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The nid
     */
    public String getNid() {
        return nid;
    }

    /**
     * 
     * @param nid
     *     The nid
     */
    public void setNid(String nid) {
        this.nid = nid;
    }

    /**
     * 
     * @return
     *     The micro
     */
    public String getMicro() {
        return micro;
    }

    /**
     * 
     * @param micro
     *     The micro
     */
    public void setMicro(String micro) {
        this.micro = micro;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
