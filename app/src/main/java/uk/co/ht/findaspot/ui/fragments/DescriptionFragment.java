package uk.co.ht.findaspot.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uk.co.ht.findaspot.R;
import uk.co.ht.findaspot.data.Amenity;
import uk.co.ht.findaspot.data.Attr;
import uk.co.ht.findaspot.data.Location;
import uk.co.ht.findaspot.data.Result;

public class DescriptionFragment extends Fragment {

    private DescriptionFragmentListener mListener;

    public DescriptionFragment() {
        // Required empty public constructor
    }
    Result myInfo;
    TextView textViewDescription;
    TextView textViewAddress;
    TextView textViewHeading;

    public static DescriptionFragment newInstance(Result currentInfoData) {
        DescriptionFragment fragment = new DescriptionFragment();
        Bundle args = new Bundle();
        args.putParcelable("info", currentInfoData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            myInfo = getArguments().getParcelable("info");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_description, container, false);
        textViewDescription = (TextView) v.findViewById(R.id.textViewDescription);
        textViewAddress = (TextView) v.findViewById(R.id.textViewAddress);
        textViewHeading = (TextView) v.findViewById(R.id.textView4);
        LoadData(myInfo.getAttr(), myInfo.getLocation());
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DescriptionFragmentListener) {
            mListener = (DescriptionFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DescriptionFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface DescriptionFragmentListener {
        void onFragmentInteraction(Uri uri);
    }


    private void LoadData(Attr attribute, Location location)
    {
        textViewDescription.setText(attribute.getDescription());
        textViewAddress.setText(location.getAll());
        textViewHeading.setText(attribute.getHeading());
    }
}
