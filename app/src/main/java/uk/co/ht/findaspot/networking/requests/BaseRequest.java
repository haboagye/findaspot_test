package uk.co.ht.findaspot.networking.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by Hanson on 08/04/2016.
 */
public abstract class BaseRequest<T, R> extends RetrofitSpiceRequest<T, R> {
    public BaseRequest(Class<T> clazz, Class<R> retrofitedInterfaceClass) {
        super(clazz, retrofitedInterfaceClass);
    }

}