
package uk.co.ht.findaspot.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.workday.postman.Postman;
import com.workday.postman.annotations.Parceled;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
@Parceled
public class ExtraGuests implements Parcelable{

    private Integer fee;
    private Integer after;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public static final Parcelable.Creator<ExtraGuests> CREATOR = Postman.getCreator(ExtraGuests.class);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Postman.writeToParcel(this,dest);
    }
    /**
     * 
     * @return
     *     The fee
     */
    public Integer getFee() {
        return fee;
    }

    /**
     * 
     * @param fee
     *     The fee
     */
    public void setFee(Integer fee) {
        this.fee = fee;
    }

    /**
     * 
     * @return
     *     The after
     */
    public Integer getAfter() {
        return after;
    }

    /**
     * 
     * @param after
     *     The after
     */
    public void setAfter(Integer after) {
        this.after = after;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
