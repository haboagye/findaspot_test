package uk.co.ht.findaspot.networking.listeners;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import uk.co.ht.findaspot.data.BoxResult;
import uk.co.ht.findaspot.ui.views.SpotListView;

/**
 * Created by Hanson on 06/10/2015.
 */
public class ResultListener implements RequestListener<BoxResult> {

        public SpotListView refFragment;

        public ResultListener(SpotListView fragment)
        {
            refFragment = fragment;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(BoxResult result) {

        }

/*

    int leagueID;

    */
/**
 * The Error target.
 *//*

    DrawerEvent errorTarget;

    */
/**
 * The Success target.
 *//*

    DrawerEvent successTarget;

    */
/**
 * The Storage helper.
 *//*

    StorageHelperInterface storageHelper;

    */
/**
 * Instantiate a new ResultListener
 *
 * @param targetStorageHelper
 *//*

    public ResultListener(StorageHelperInterface targetStorageHelper, int sportID) {
        super();
        storageHelper = targetStorageHelper;
        this.leagueID = sportID;
    }


    @Override
    public void onRequestFailure(SpiceException spiceException) {
        Log.e("SUBLEAGUE", " failed");
    }

    @Override
    public void onRequestSuccess(Division division) {
        if (division.getSubLeagues() != null) {
            for (SubLeague subLeague : division.getSubLeagues()) {
                if (subLeague.getGroups() != null) {
                    StoreGroups(subLeague.getGroups(), subLeague.getID(), true);
                } else {
                    if (subLeague.getTeams() != null) {
                        storageHelper.AddUpdateTeam(subLeague.getTeams(), subLeague.getID(), division.getID(), leagueID);
                    }
                }
            }
        } else {
            StoreGroups(division.getGroups(), division.getID(), false);
        }
    }

    private Integer subID = -1;

    private void StoreGroups(List<Group> subDivisions,
                             int divisionID, boolean saveInfo) {
        for (Group group : subDivisions) {

            if (saveInfo) {
                // Save subdivision
                storageHelper.AddUpdateSubDivisions(group, leagueID, divisionID);
            }

            if (group.getGroups() != null) {
                subID = group.getID();
                StoreGroups(group.getGroups(), divisionID, false);
            } else {

                Integer subDivID = group.getID();
                if (subID >= 0)
                    subDivID = subID;
                storageHelper.AddUpdateTeam(group.getTeams(), subDivID, divisionID, leagueID);
            }
        }
        subID = -1;
    }


    public DrawerEvent getSuccessTarget() {
        return successTarget;
    }

    public void setSuccessTarget(DrawerEvent successTarget) {
        this.successTarget = successTarget;
    }

    public DrawerEvent getErrorTarget() {
        return errorTarget;
    }

    public void setErrorTarget(DrawerEvent errorTarget) {
        this.errorTarget = errorTarget;
    }
*/

}
