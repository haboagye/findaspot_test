Find A Spot Application

This is based on a free REST APO from https://market.mashape.com/zilyo/zilyo# 

it returns a wide range of accomodation based on your location

For more details see https://market.mashape.com/zilyo/zilyo/overview


# How to use the app #

Open the app and scroll the map to your target location or use the "My Location" feature of Google maps to zoom in to your location.

Then click on the map anywhere. A progress bar will appear whilst it loads the accomodation in your view point.

You can then click on the list at the bottom to view more details

TOD (if there was more time):

* Add database storage of results
* Add caching based on id's
* Add gestures to swipe back to map
* Add error messages
* Display the review info
* Display the Amenities info
* Tidy up code
* Change colour scheme
* Allow user to click on an item on the map (not just in the list at the bottom) and then view details - or filter the list to the one selected

I concentrated on a range of techniques, though UX could have been improved , i only had about 14 hours work on it in total so some UX items were minimised

The app has been tested on a Nexus 4 running Android Marshmallow (API 23) only